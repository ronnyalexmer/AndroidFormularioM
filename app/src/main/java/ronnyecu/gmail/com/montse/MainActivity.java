package ronnyecu.gmail.com.montse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.validator.routines.EmailValidator;

public class MainActivity extends AppCompatActivity {

    EditText nombre;
    EditText email;
    EditText cumple;
    Button enviar;
    EmailValidator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        find();
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, verifyMail(email.getText().toString()) +"", Toast.LENGTH_LONG).show();
            }
        });

    }

    void find() {
        nombre = (EditText) findViewById(R.id.nombre);
        email = (EditText) findViewById(R.id.email);
        cumple = (EditText) findViewById(R.id.birthday);
        enviar = (Button) findViewById(R.id.sendButton);
    }

    boolean verifyMail(String email) {
        validator = EmailValidator.getInstance();
        return validator.isValid(email);

    }
}
